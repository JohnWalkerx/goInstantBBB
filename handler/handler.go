package handler

import (
	"log"
	"sync"

	"codeberg.org/JohnWalkerx/goInstantBBB/cloudProvider"
	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	"codeberg.org/JohnWalkerx/goInstantBBB/dns"
	"codeberg.org/JohnWalkerx/goInstantBBB/sshClient"
)

type State int

const (
	EvStateUnknown State = iota
	EvStateNotRunning
	EvStateStartingUp
	EvStateRunning
	EvStateShuttingDown
)

type Handler struct {
	cloud            *cloudProvider.CloudProvider
	dns              *dns.Dns
	serverState      State
	serverStateMutex sync.Mutex
	execMutex        sync.Mutex
}

func NewHandler() *Handler {
	object := &Handler{
		cloud: cloudProvider.NewCloudProviderObject(),
		dns:   dns.NewDnsObject(),
	}

	// Init server state
	existing, _, _ := object.cloud.IsBBBServerExisting()

	if existing {
		object.serverState = EvStateRunning
	} else {
		object.serverState = EvStateNotRunning
	}

	return object
}

func (h *Handler) GetState() State {
	h.serverStateMutex.Lock()
	defer h.serverStateMutex.Unlock()
	return h.serverState
}

func (h *Handler) setState(state State) {
	h.serverStateMutex.Lock()
	defer h.serverStateMutex.Unlock()
	h.serverState = state
}

func (h *Handler) StartupBBBServer() error {
	h.execMutex.Lock()
	defer h.execMutex.Unlock()

	log.Println("Startup BBB server.")
	h.setState(EvStateStartingUp)

	existsAlready, ipv4, ipv6, err := h.cloud.CreateBBBServer()

	if err == nil {
		zoneId := h.dns.ZoneId(config.Get().Dns.ZoneName)
		h.dns.CreateOrUpdateRecords(zoneId, config.Get().Dns.RecordName, ipv4, ipv6)
	} else {
		h.setState(EvStateUnknown)
		return err
	}

	if !existsAlready {
		client, _ := sshClient.NewSshClientObject(ipv4, config.Get().BBBServer.SSHPort)

		err := client.ConfigureBBBServer()
		if err != nil {
			h.setState(EvStateUnknown)
			return err
		}

		// Only restore database if configured
		if config.Get().Greenlight.RestoreDatabase {
			err := client.RestoreGreenlight()
			if err != nil {
				log.Println("Error: Restore of Greenlight database failed: ", err)
				h.setState(EvStateUnknown)
				return err
			}
		}
	}

	log.Println("Creation of BBB server done.")
	h.setState(EvStateRunning)

	return err
}

func (h *Handler) DeleteBBBServer() error {
	h.execMutex.Lock()
	defer h.execMutex.Unlock()

	log.Println("Delete BBB server.")
	h.setState(EvStateShuttingDown)

	exists, ipv4, _ := h.cloud.IsBBBServerExisting()
	if !exists {
		log.Println("Warning: BBBServer doesn't exist. Can not be deleted.")
		h.setState(EvStateNotRunning)
		return nil
	}

	// Always create Backup of Greenlight database
	client, _ := sshClient.NewSshClientObject(ipv4, config.Get().BBBServer.SSHPort)

	err := client.BackupGreenlight()
	if err != nil {
		log.Println("Error: Backup of Greenlight database failed: ", err)
	}

	err = h.cloud.DeleteBBBServer()

	if err != nil {
		log.Println("Error: Couldn't delete BBBServer:", err)
		h.setState(EvStateUnknown)
		return err
	}

	zoneId := h.dns.ZoneId(config.Get().Dns.ZoneName)
	h.dns.DeleteRecords(zoneId, config.Get().Dns.RecordName)

	log.Println("Deletion of BBB server done.")
	h.setState(EvStateNotRunning)

	return nil
}
