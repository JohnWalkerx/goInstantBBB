package dns

import (
	"context"
	"fmt"
	"log"

	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	hdns "github.com/panta/go-hetzner-dns"
)

type Dns struct {
	hdnsClient *hdns.Client
}

func NewDnsObject() *Dns {
	newObject := &Dns{
		hdnsClient: &hdns.Client{ApiKey: config.Get().Dns.ApiKey},
	}

	return newObject
}

func (d *Dns) GetDnsRecords() {
	zonesResponse, err := d.hdnsClient.GetZones(context.Background(), "", "", 1, 100)

	if err != nil {
		log.Fatal(err)
	}

	for _, zone := range zonesResponse.Zones {
		fmt.Printf("Zone name: %s\n", zone.Name)
	}

	records, _ := d.hdnsClient.GetRecords(context.Background(), zonesResponse.Zones[0].ID, 1, 100)

	for _, record := range records.Records {
		fmt.Printf("Zone Records name: %s\n", record.Name)
	}
}

func (d *Dns) DnsRecords(zoneId string, recordName string) map[string]hdns.Record {
	records, errRecords := d.hdnsClient.GetRecords(context.Background(), zoneId, 1, 100)

	if errRecords != nil {
		log.Fatal(errRecords)
	}

	resultRecords := make(map[string]hdns.Record)

	// Go over all records an search for the correct one
	for _, record := range records.Records {
		if record.Name != recordName {
			continue
		}

		if record.Type == "A" {
			resultRecords["A"] = record
		} else if record.Type == "AAAA" {
			resultRecords["AAAA"] = record
		}
	}

	return resultRecords
}

func (d *Dns) ZoneId(zoneName string) string {
	zonesResponse, errZone := d.hdnsClient.GetZones(context.Background(), zoneName, "", 1, 100)

	if errZone != nil {
		log.Fatal(errZone)
	}

	if len(zonesResponse.Zones) == 0 {
		log.Fatalln("No zones found")
	}

	if len(zonesResponse.Zones) > 1 {
		log.Fatalln("More zones than 1 found")
	}

	return zonesResponse.Zones[0].ID
}

func (d *Dns) CreateOrUpdateRecords(zoneId string, name string, ipv4 string, ipv6 string) bool {
	log.Printf("Update DNS of record '%s' with IPv4: %s and IPv6: %s\n", name, ipv4, ipv6)

	recordRequestIpv4 := hdns.RecordRequest{
		ZoneID: zoneId,
		Type:   "A",
		Name:   name,
		Value:  ipv4,
		TTL:    600,
	}
	recordResponseIpv4, errIpv4 := d.hdnsClient.CreateOrUpdateRecord(context.Background(), recordRequestIpv4)

	if errIpv4 != nil {
		log.Fatalln(errIpv4)
	}

	recordRequestIpv6 := hdns.RecordRequest{
		ZoneID: zoneId,
		Type:   "AAAA",
		Name:   name,
		Value:  ipv6,
		TTL:    600,
	}
	recordResponseIpv6, errIpv6 := d.hdnsClient.CreateOrUpdateRecord(context.Background(), recordRequestIpv6)

	if errIpv6 != nil {
		log.Fatalln(errIpv6)
	}

	if recordResponseIpv4.Record.Value != ipv4 || recordResponseIpv6.Record.Value != ipv6 {
		return false
	}

	return true
}

func (d *Dns) DeleteRecords(zoneId string, name string) bool {
	log.Printf("Delete DNS records of %s.%s\n", name, zoneId)

	result := true

	records := d.DnsRecords(zoneId, name)

	recordA, ok := records["A"]
	if ok {
		errA := d.hdnsClient.DeleteRecord(context.Background(), recordA.ID)
		if errA != nil {
			log.Printf("error on delete A record %s", errA)
			result = false
		}
	}

	recordAAAA, ok := records["AAAA"]
	if ok {
		errAAAA := d.hdnsClient.DeleteRecord(context.Background(), recordAAAA.ID)
		if errAAAA != nil {
			log.Printf("error on delete A record %s", errAAAA)
			result = false
		}
	}

	return result
}
