package main

import (
	"fmt"

	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	"codeberg.org/JohnWalkerx/goInstantBBB/server"
	"codeberg.org/JohnWalkerx/goInstantBBB/userManager"
)

func main() {
	fmt.Println("Main is starting.")

	config.ReadConfiguration()

	userManager.SetPasswordFilePath("./password.json")
	userManager.ReadPasswordFile()

	userManager.CreateUser(config.Get().ControlInterface.AdminUsername, config.Get().ControlInterface.AdminPassword)

	srv := server.NewServerObject()
	srv.RegisterApiHandlers()
	srv.RegisterUiHandlers()
	srv.ListenAndServe(":3000")

}
