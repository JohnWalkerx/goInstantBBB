package cloudProvider

import (
	"context"
	"fmt"
	"log"
	"strconv"

	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	"github.com/hetznercloud/hcloud-go/hcloud"
)

type CloudProvider struct {
	hcloudClient *hcloud.Client
}

func NewCloudProviderObject() *CloudProvider {
	newObject := &CloudProvider{
		hcloudClient: hcloud.NewClient(hcloud.WithToken(config.Get().BBBServer.ApiKey)),
	}

	return newObject
}

// Creates the BBB Server
func (s *CloudProvider) CreateBBBServer() (exists bool, ipv4 string, ipv6 string, err error) {

	// Check if server already exists
	if existing, ipv4Existing, ipv6Existing := s.IsBBBServerExisting(); existing {
		exists = true
		ipv4 = ipv4Existing
		ipv6 = ipv6Existing
		return
	}

	location, _, _ := s.hcloudClient.Location.GetByName(context.Background(), config.Get().BBBServer.Location)
	image, _, _ := s.hcloudClient.Image.GetByName(context.Background(), "big-blue-button")
	serverType, _, _ := s.hcloudClient.ServerType.GetByName(context.Background(), config.Get().BBBServer.ServerType)
	sshkey, _, _ := s.hcloudClient.SSHKey.Get(context.Background(), config.Get().BBBServer.SSHKeyName)
	sshkeyLogin, _, _ := s.hcloudClient.SSHKey.Get(context.Background(), config.Get().BBBServer.SSHKeyLoginName)
	startAfterCreate := true

	createOps := hcloud.ServerCreateOpts{
		Name:             s.ServerName(),
		Labels:           map[string]string{"bbbserver": ""},
		Location:         location,
		Image:            image,
		ServerType:       serverType,
		SSHKeys:          []*hcloud.SSHKey{sshkey, sshkeyLogin},
		StartAfterCreate: &startAfterCreate,
		UserData:         "#cloud-config\n\nruncmd:\n - \"sed -i 's/#Port 22/Port " + strconv.FormatUint(uint64(config.Get().BBBServer.SSHPort), 10) + "/g' /etc/ssh/sshd_config\"\n - \"sed -i 's/#PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config\"\n - service ssh reload",
	}

	log.Println("Create BBB server.")
	server, _, err := s.hcloudClient.Server.Create(context.Background(), createOps)

	if err != nil {
		log.Fatalf("error creating server: %s\n", err)
		return false, "", "", err
	}

	ipv4 = server.Server.PublicNet.IPv4.IP.String()
	ipv6 = server.Server.PublicNet.IPv6.IP.String() + "1"

	fmt.Printf("server: %v\n", server)

	return
}

// Deletes the BBB Server
func (s *CloudProvider) DeleteBBBServer() error {
	log.Println("Delete BBB server.")
	server, _, errGetServer := s.hcloudClient.Server.GetByName(context.Background(), s.ServerName())

	if errGetServer != nil {
		log.Fatalf("error get server: %s\n", errGetServer)
	}

	if server == nil {
		// No server to delete
		log.Println("No server to delete")
		return nil
	}

	_, errDeleteServer := s.hcloudClient.Server.Delete(context.Background(), server)

	if errDeleteServer != nil {
		log.Fatalf("error delete server: %s\n", errDeleteServer)
		return errDeleteServer
	}

	return nil
}

// Checks if the BBB Server is already existing
func (s *CloudProvider) IsBBBServerExisting() (success bool, ipv4 string, ipv6 string) {
	// Check if server already exists
	checkServer, _, err := s.hcloudClient.Server.GetByName(context.Background(), s.ServerName())

	if err != nil {
		log.Println("error get server by name: ", err)
		success = false
		return
	}

	if checkServer != nil {
		// Server exists already
		log.Println("server already exist")
		success = true
		// TODO: Create a function to get IP addresses
		ipv4 = checkServer.PublicNet.IPv4.IP.String()
		ipv6 = checkServer.PublicNet.IPv6.IP.String() + "1"
		return
	}

	success = false
	return
}

func (s *CloudProvider) ServerName() string {
	return config.Get().BBBServer.InstanceName
}
