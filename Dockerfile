FROM golang:1.17 AS certs

FROM scratch

# copy certs from golang:1.17 image
COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

# Copy prebuild Go binary into container
COPY ./goInstantBBB /goInstantBBB

# Copy UI static files into container
COPY ./ui/public/ /ui/public/

# Golang Webserver exposes on port 3000
EXPOSE 3000

# Data directory for config and other stuff
VOLUME [ "/data" ]

# Run application
CMD ["/goInstantBBB"]