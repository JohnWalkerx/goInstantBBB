package config

import (
	"log"

	"github.com/spf13/viper"
)

type Config struct {
	General          GeneralConfig
	ControlInterface ControlInterfaceConfig
	Dns              DnsConfig
	BBBServer        BBBServerConfig
	BigBlueButton    BigBlueButtonConfig
	Greenlight       GreenlightConfig
}

type GeneralConfig struct {
	DataDirectory string
	JwtSecret     string
}

type ControlInterfaceConfig struct {
	AdminUsername string
	AdminPassword string
}

type DnsConfig struct {
	ApiKey     string
	ZoneName   string
	RecordName string
}

type BBBServerConfig struct {
	ApiKey          string
	Location        string
	ServerType      string
	SSHPort         uint
	SSHKeyName      string
	SSHKeyLoginName string
	InstanceName    string
}

type BigBlueButtonConfig struct {
	StunServer              string
	TurnServer              string
	TurnServerPort          uint
	TurnServerKey           string
	EnableMultipleKurentos  bool
	AllowStartStopRecording bool
	DisableRecordingDefault bool
}

type GreenlightConfig struct {
	LetsEncryptEmail    string
	AdminUserName       string
	AdminEmail          string
	AdminPassword       string
	Smtp                GreenlightSmtpConfig
	DefaultRegistration string
	RestoreDatabase     bool
}

type GreenlightSmtpConfig struct {
	Server        string
	Port          uint
	Domain        string
	Username      string
	Password      string
	Auth          string
	StartTlsAuto  bool
	Tls           bool
	Sender        string
	TestRecipient string
}

// Global variable for other parts to get the current config
var gConfig Config

func Get() Config {
	return gConfig
}

func ReadConfiguration() bool {
	log.Println("Reading configuration")

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/data")

	setConfigDefaults()

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s\n", err)
	}

	err := viper.Unmarshal(&gConfig)
	if err != nil {
		log.Fatalf("Unable to decode into struct, %v\n", err)
	}

	return true
}

func setConfigDefaults() {
	viper.SetDefault("General.DataDirectory", "/data/")
	viper.SetDefault("BBBServer.Location", "nbg1")
	viper.SetDefault("BBBServer.ServerType", "cpx41")
	viper.SetDefault("BBBServer.InstanceName", "InstantBBB")
}
