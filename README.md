# goInstantBBB - Your instant BigBlueButton server

[![status-badge](https://ci.codeberg.org/api/badges/JohnWalkerx/goInstantBBB/status.svg)](https://ci.codeberg.org/JohnWalkerx/goInstantBBB)
[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/johnwalkerx/goinstantbbb?label=Docker%20Hub&logo=Docker&sort=semver)](https://hub.docker.com/r/johnwalkerx/goinstantbbb)

**2022-09: This project is not useable in this state because it uses the Hetzner BigBlueButton-Image. But Hetzner decided to stop providing this image. So we would need to build our own image.**

goInstantBBB lets you create and reconfigure or delete an BigBlueButton (BBB) cloud server with one click.
It is a web service with provides a little user interface to control the creation and deletion of the BBB server.

**Note:** This project is in a very early state. API can change on every version.

## Why?

If you need a BigBlueButton server only several hours a day or week, most of the time it is idle.
Paying 24/7 for a server can be very expensive. Especially if you use it for your personal meetings.
Also it saves energy and resources - more important than ever before.

So the idea came up to just run the server when we need it.
And with pay-per-hour cloud offers this is possible.
Also to make it easy for non-technical persons it must be easy to use.

## How does it work?

It uses the Hetzner Cloud API to create the "BigBlueButton-App" of Hetzner. So we don't need to install BigBlueButton from scratch.

And it uses the Hetzner DNS API to create/set the IP address of the created server to a configured record.

Then it runs Hetzner's setup scripts to spin up BigBlueButton.
It also setups the SMTP server for Greenlight and some other stuff.
You can also specify your own STUN and TURN servers.

Also it tries to restore the Greenlight database which got backed up on the last deletion of the server (if configured).
So the rooms, their urls, users and so on remains the same.
(Recordings will not be saved at the moment)

When the server gets shutdown it creates a database dump of Greenlight, downloads them and deletes the server.

## Prerequisites

You need following prerequisites:

- Hetzner Cloud Account
- Domain whose DNS is managed by the Hetzner DNS Console.
- Server which can run a docker container and a reverse proxy.

## Installation

You can get the application packaged as docker container from [Docker Hub](https://hub.docker.com/r/johnwalkerx/goinstantbbb).

You need to setup a reverse proxy to do all the HTTPS stuff by yourself.

## Configuration

### Create a configuration file

You can copy the [config-templage.yml](config-template.yaml) to `config.yml` and put it into the `/data` volume or your bind directory.

Set all usernames, passwords, smtp server credentials.
If you have a separate turn server setup those hostnames and credentials.

Also generate a random secret for the JWT (JSON Web Tokens):

```bash
openssl rand -hex 30
```

and set it to the `JwtSecret` in the `General` section.

### Create SSH keypair for application to setup remote server

You need to create a SSH keypair to goInstantBBB without a passphrase which the application can use to login into the remote server.

For this run:

```bash
ssh-keygen -t rsa -b 4096 -q -f "id_rsa_bbb" -N ""
```

This creates a private key `id_rsa_bbb` and a public key `id_rsa_bbb.pub`.

Both files needs to be copied to the `/data` volume or your bind directory.

### Setup Hetzner cloud project

If you don't have already an account at [Hetzner](https://accounts.hetzner.com/login) just create one.

The create a new project in the [cloud console](https://console.hetzner.cloud/projects).

#### SSH-Key

Switch to `Security - SSH-Keys` and add a new SSH-Key.
Name it `instantBBBKey` (like the assign value to `SSHKeyLoginName` in the `BBBServer` section of the config) and copy the content of the `id_rsa_bbb.pub` key.

You can add your own public key to the project and set the name of this key in the `config.yml` to `SSHKeyName` in the `BBBServer` section.
Then this key is also added to the server while creation.
So you can also login to the server.

#### API Token

Switch to `Security - API-Tokens` and add a new API token.
Choose a name and check the `Read & Write` permission.

Copy the generated API token and put it in the `config.yml` in `ApiKey` in the `BBBServer` section.

### Setup [Hetzner DNS Console](https://dns.hetzner.com/)

Go to your Hetzner DNS Console (needs to be setup before) and
generate an API token.

Copy the generated API token and put it in the `config.yml` in `ApiKey` in the `Dns` section.

## Build

Make sure you installed golang.

Then run:

```bash
cd goInstantBBB
go build .
```

To start run:

```bash
go run .
```

## Development

If you use Visual Studio Code you can install the golang extension and just run the debugger with F5.

Make sure you create a valid config.yml and ssh-key-pair in the working directory.

To test functions it is useful to have a separate cloud project at hetzner.
Also use a different DNS record. The best would be when you also have a different domain.
Because Let's encrypt have a [rate limit](https://letsencrypt.org/docs/rate-limits/) and you could block
your main dns for a week for new certificate renewal.

## Licence

The source code is licensed under the [AGPL](LICENSE).
