package userManager

import (
	"encoding/json"
	"log"
	"os"
)

type User struct {
	Username      string `json:"username"`
	PasswordHash  string `json:"passwordhash"`
	HashAlgorithm string `json:"hashAlgorithm"`
}

func ReadPasswordFile() error {
	file, err := os.Open(g_passwordFilePath)
	if err != nil {
		log.Println("Can't open and read password file: ", err)
		return err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)

	users := []User{}

	// Read the array open bracket
	decoder.Token()

	data := User{}
	for decoder.More() {
		decoder.Decode(&data)

		users = append(users, data)
	}

	g_users = users

	return nil
}

func WritePasswordFile() error {
	file, err := os.Create(g_passwordFilePath)
	if err != nil {
		log.Println("Can't open and write password file: ", err)
		return err
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	err = encoder.Encode(g_users)
	if err != nil {
		log.Println("Can't write password file: ", err)
		return err
	}

	return nil
}
