package userManager

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadPasswordFile(t *testing.T) {
	// Prepare json File
	const filename = "passwordtest.json"

	jsonFile := "[{\"username\":\"user1\",\"passwordhash\":\"jasdöfaslk\",\"hashAlgorithm\":\"bcrypt\"}]"
	err := ioutil.WriteFile(filename, []byte(jsonFile), 0644)
	if err != nil {
		t.Log("Text password file couldn't written: ", err)
		t.FailNow()
	}

	SetPasswordFilePath(filename)

	err = ReadPasswordFile()
	if err != nil {
		t.Log("Failed to read password file: ", err)
		t.FailNow()
	}

	assert.Equal(t, 1, len(g_users), "User count in internal storage not equal.")

	actualUser := g_users[0]

	expectedUser := User{
		Username:      "user1",
		PasswordHash:  "jasdöfaslk",
		HashAlgorithm: "bcrypt",
	}

	assert.Equal(t, expectedUser, actualUser, "User object not equal.")

}
