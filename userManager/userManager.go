package userManager

import (
	"errors"
	"log"
	"sync"
)

var g_passwordFilePath string
var g_users []User
var g_mutexUsers = &sync.Mutex{}

func SetPasswordFilePath(filepath string) {
	g_passwordFilePath = filepath
}

// type userManager struct {
// 	passwordFilePath string
// 	users            []User
// 	mutexUsers       *sync.Mutex
// }

// func NewUserManagerObject(passwordFilePath string) (*userManager, error) {
// 	newObject := &userManager{
// 		passwordFilePath: passwordFilePath,
// 		mutexUsers:       &sync.Mutex{},
// 	}

// 	return newObject, nil
// }

// Needed Functions:
/*

UserRoles:
- admin
- user

Password handling:
- Generate individual salt for every user.
- Hash password+salt
- Store salt and hash for every user

File storage:
- Write json file with stored users
e.g.
{
	"users": [
		{
			"username": "fuu",
			"passwordHash": "adsföasfksö",
			"salt": "asdfjaöslsdfkdsfj",
			"hashAlgorithm": "sha512"
		}
	]
}

Needed Function:
- Create New User (provide username and password)
- Delete User
- Set User Password
- Get users list
- Get userinfo
- authUser (provide username, password, needed role) (return true/false)

*/

func CreateUser(username string, password string) error {
	g_mutexUsers.Lock()
	defer g_mutexUsers.Unlock()

	if username == "" {
		err := errors.New("Username is empty!")
		log.Println(err)
		return err
	}

	// Check if user already exists
	if existsUser(username) {
		err := errors.New("User '" + username + "' exists already!")
		log.Println(err)
		return err
	}

	passwordHash, err := hashPassword(password)
	if err != nil {
		log.Println("Password of user '", username, "' couldn't be hashed. Exit CreateUser function.: ", err)
		return err
	}

	newUser := User{
		Username:      username,
		PasswordHash:  passwordHash,
		HashAlgorithm: usedHashAlgorithm(),
	}

	g_users = append(g_users, newUser)

	WritePasswordFile()

	return nil
}

func DeleteUser(username string) error {
	g_mutexUsers.Lock()
	defer g_mutexUsers.Unlock()

	for index, user := range g_users {
		if user.Username == username {
			g_users = removeUserIndex(g_users, index)
			WritePasswordFile()
			return nil
		}
	}

	return errors.New("No user with username '" + username + "' found to delete")
}

func IsUserAuthentificated(username string, password string) bool {
	g_mutexUsers.Lock()
	defer g_mutexUsers.Unlock()

	user, err := UserByUsername(username)
	if err != nil {
		return false
	}

	return doPasswordsMatch(user.PasswordHash, password)
}

func SetUserPassword(username string, password string) error {
	g_mutexUsers.Lock()
	defer g_mutexUsers.Unlock()

	user, err := UserByUsername(username)
	if err != nil {
		log.Println("User password couldn't be set: ", err)
		return err
	}

	passwordHash, err := hashPassword(password)
	if err != nil {
		log.Println("Password of user '", username, "' couldn't be hashed. Exit CreateUser function.: ", err)
		return err
	}

	user.PasswordHash = passwordHash

	return setUser(user)
}

func existsUser(username string) bool {
	for _, user := range g_users {
		if user.Username == username {
			return true
		}
	}

	return false
}

func ExistingUsernames() []string {
	usernames := []string{}

	for _, user := range g_users {
		usernames = append(usernames, user.Username)
	}

	return usernames
}

func UserByUsername(username string) (User, error) {
	for _, user := range g_users {
		if user.Username == username {
			return user, nil
		}
	}

	return User{}, errors.New("No user found by name: " + username)
}

func setUser(newUser User) error {
	for i, user := range g_users {
		if user.Username == newUser.Username {
			g_users[i] = newUser
			WritePasswordFile()
			return nil
		}
	}

	return errors.New("found no existing user")
}

func removeUserIndex(u []User, index int) []User {
	u[index] = u[len(u)-1]
	return u[:len(u)-1]
}
