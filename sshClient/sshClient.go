package sshClient

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	"golang.org/x/crypto/ssh"
)

// TODO:
// - Check if local ssh key pair exists. If not create new one with go.
// - Check if local ssh key pair is already present in Hetzner Cloud project. If not upload public key there.
// - Create function to setup BBB server.
//   - Execute commands out of goInstantBBB
//     -> Better because we need to e.g. adjust the TURN-Server-Key
//   - Or create separate Repo which contains all relevant scripts and files and clone it by command on the server
//     and just execute these scripts

// Create key by: ssh-keygen -t rsa -b 4096 -q -f "id_rsa_bbb" -N ""
// Or in Golang see https://stackoverflow.com/questions/21151714/go-generate-an-ssh-public-key

type sshClient struct {
	ipAddress string
	port      uint
}

func NewSshClientObject(ipAddress string, port uint) (*sshClient, error) {
	newObject := &sshClient{
		ipAddress: ipAddress,
		port:      port,
	}

	return newObject, nil
}

// Creates a new SSH Client.
// If client is not needed anymore be sure to call client.Close()
func (m *sshClient) newSshClient() (*ssh.Client, error) {
	log.Println("Create new SSH client.")

	pathOfKeyFile := filepath.Join(filepath.Dir(config.Get().General.DataDirectory), "id_rsa_bbb")

	key, err := ioutil.ReadFile(pathOfKeyFile)
	if err != nil {
		log.Println("unable to read private key:", err)
		return nil, err
	}

	// Create the Signer for this private key.
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Println("unable to parse private key:", err)
		return nil, err
	}

	config := &ssh.ClientConfig{
		User: "root",
		Auth: []ssh.AuthMethod{
			// Use the PublicKeys method for remote authentication.
			ssh.PublicKeys(signer),
		},
		// NOTE: On every creation of the server it generates a new HostKey, so it is difficult to check it.
		// HostKeyCallback: ssh.FixedHostKey(hostKey),
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// Connect to the remote server and perform the SSH handshake.
	connectionStr := m.ipAddress + ":" + strconv.FormatUint(uint64(m.port), 10)
	var client *ssh.Client

	const retryNumber = 5

	for i := 0; i < retryNumber; i++ {
		client, err = ssh.Dial("tcp", connectionStr, config)

		// Retry connection 5 times with wait time
		if err != nil {
			log.Printf("%d. connection attempt of %d was refused. Retry in 2 seconds...", i+1, retryNumber)
			time.Sleep(5 * time.Second)
			if i == retryNumber-1 {
				log.Println("unable to connect after:", err)
				return nil, err
			}
		}
	}

	return client, err
}

func (m *sshClient) runMultipleCommands(client *ssh.Client, commands []string) (output string, err error) {
	session, err := client.NewSession()
	if err != nil {
		log.Println("Failed to create session:", err)
		return "", err
	}
	defer session.Close()

	// StdinPipe for commands
	stdin, err := session.StdinPipe()
	if err != nil {
		log.Println("Failed to get StdinPipe:", err)
		return "", err
	}

	// Uncomment to store output in variable
	var buffer bytes.Buffer
	session.Stdout = &buffer
	session.Stderr = &buffer

	// Start remote shell
	err = session.Shell()
	if err != nil {
		log.Println("Error: Couldn't start remote shell:", err)
		return "", err
	}

	for _, cmd := range commands {
		_, err = fmt.Fprintf(stdin, "%s\n", cmd)
		if err != nil {
			log.Println("Failed to send command", cmd, "to Stdin:", err)
		}
	}

	// Wait for session to finish
	// TODO: Here we block until the script is completly executed. Can we do this async? Needed?
	err = session.Wait()
	if err != nil {
		log.Println("Failed to execute all commands correctly:", err)
		log.Println("Print Console output:")
		fmt.Println(buffer.String())
		return buffer.String(), err
	}

	// For Testing: Print output of commands
	fmt.Println(buffer.String())

	return buffer.String(), nil
}

func (m *sshClient) runSingleCommand(client *ssh.Client, command string) (output string, err error) {
	session, err := client.NewSession()
	if err != nil {
		log.Println("Failed to create session:", err)
		return "", err
	}
	defer session.Close()

	// Once a Session is created, you can execute a single command on
	// the remote side using the Run method.
	var buffer bytes.Buffer
	session.Stdout = &buffer

	if err := session.Run(command); err != nil {
		log.Println("Failed to run: " + err.Error())
		log.Println("Print Console output:")
		fmt.Println(buffer.String())
		return buffer.String(), err
	}

	fmt.Println(buffer.String())

	return buffer.String(), nil
}

func SshKeyExists() bool {
	pathOfKeyFile := filepath.Join(filepath.Dir(config.Get().General.DataDirectory), "id_rsa_bbb")
	_, err := os.Stat(pathOfKeyFile)
	return !errors.Is(err, os.ErrNotExist)
}
