package sshClient

import (
	"context"
	"log"
	"os"

	"github.com/bramvdbogaerde/go-scp"
	"golang.org/x/crypto/ssh"
)

func downloadFileFromServer(client *ssh.Client, remotePath string, localPath string) error {
	scpClient, err := scp.NewClientBySSH(client)
	if err != nil {
		log.Println("Error creating new SCP Client: ", err)
		return err
	}
	defer scpClient.Close()

	f, err := os.OpenFile(localPath, os.O_RDWR|os.O_CREATE, 0750)
	if err != nil {
		log.Println("Error: Couldn't open the output file: ", err)
		return err
	}
	defer f.Close()

	err = scpClient.CopyFromRemote(context.Background(), f, remotePath)
	if err != nil {
		log.Println("Error: Copy from remote failed: ", err)
		return err
	}

	return err
}

func uploadFileFromServer(client *ssh.Client, localPath string, remotePath string) error {
	scpClient, err := scp.NewClientBySSH(client)
	if err != nil {
		log.Println("Error creating new SCP Client: ", err)
		return err
	}
	defer scpClient.Close()

	f, err := os.Open(localPath)
	if err != nil {
		log.Println("Error: Couldn't open the input file: ", err)
		return err
	}
	defer f.Close()

	err = scpClient.CopyFile(context.Background(), f, remotePath, "0755")
	if err != nil {
		log.Printf("Error: Copy file '%s' to remote '%s' failed: %s\n", localPath, remotePath, err)
		return err
	}

	return err
}
