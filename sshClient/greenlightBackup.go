package sshClient

import (
	"errors"
	"log"
	"os"
	"path/filepath"

	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	"golang.org/x/crypto/ssh"
)

func (m *sshClient) BackupGreenlight() error {
	log.Println("Backup Greenlight.")

	client, err := m.newSshClient()
	if err != nil {
		log.Println("Couldn't create SSH client:", err)
		return err
	}
	defer client.Close()

	err = m.executeCreateGreenlightDatabaseDump(client)
	if err != nil {
		log.Println("Error: Create Greenlight database dump wasn't successful.")
		return err
	}

	return err
}

func (m *sshClient) RestoreGreenlight() error {
	log.Println("Restore Greenlight.")

	client, err := m.newSshClient()
	if err != nil {
		log.Println("Couldn't create SSH client:", err)
		return err
	}
	defer client.Close()

	err = m.executeRestoreGreenlightDatabaseDump(client)
	if err != nil {
		log.Println("Error: Restore Greenlight database dump wasn't successful.")
		return err
	}

	return err
}

func (m *sshClient) executeCreateGreenlightDatabaseDump(client *ssh.Client) error {
	log.Println("Execute Greenlight database dump.")

	dumpCommand := "docker exec greenlight_db_1 /bin/bash -c \"pg_dump --username=postgres --dbname=greenlight_production\" > /root/db.sql"

	_, err := m.runSingleCommand(client, dumpCommand)
	if err != nil {
		log.Println("Error: Execution of Creating Greenlight database dump failed: ", err)
		return err
	}

	log.Println("Download Greenlight database dump file.")
	err = downloadFileFromServer(client, "/root/db.sql", filepath.Join(filepath.Dir(config.Get().General.DataDirectory), "greenlight-db-dump.sql"))
	if err != nil {
		log.Println("Error: Downloading of created database dump failed: ", err)
	}

	return err
}

func (m *sshClient) executeRestoreGreenlightDatabaseDump(client *ssh.Client) error {
	databaseRemotePath := "/root/greenlight/db/production/db.sql"
	log.Println("Execute Greenlight database restore.")

	log.Println("Check if database dump exists.")
	if dumpExists := fileExists("greenlight-db-dump.sql"); !dumpExists {
		log.Println("Error: Database dump doesn't exist.")
		return errors.New("database dump doesn't exist")
	}

	log.Println("Copy database dump to server.")
	err := uploadFileFromServer(client, filepath.Join(filepath.Dir(config.Get().General.DataDirectory), "greenlight-db-dump.sql"), databaseRemotePath)
	if err != nil {
		log.Println("Error: Upload of database dump failed: ", err)
		return err
	}

	restoreCommands := []string{
		"docker-compose -f /root/greenlight/docker-compose.yml stop app",
		"docker exec greenlight_db_1 psql --username=postgres -c \"DROP DATABASE greenlight_production\"",
		"docker exec greenlight_db_1 psql --username=postgres -c \"CREATE DATABASE greenlight_production\"",
		"docker exec greenlight_db_1 /bin/bash -c \"psql --username=postgres --dbname=greenlight_production < /var/lib/postgresql/data/db.sql\"",
		"rm " + databaseRemotePath,
		"docker-compose -f /root/greenlight/docker-compose.yml start app",
		"exit",
	}
	_, err = m.runMultipleCommands(client, restoreCommands)
	if err != nil {
		log.Println("Error: Execution of Creating Greenlight database dump failed: ", err)
		return err
	}

	return err
}

func fileExists(filename string) bool {
	pathOfKeyFile := filepath.Join(filepath.Dir(config.Get().General.DataDirectory), filename)
	_, err := os.Stat(pathOfKeyFile)
	return !errors.Is(err, os.ErrNotExist)
}
