package sshClient

import (
	"log"
	"strconv"

	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	"golang.org/x/crypto/ssh"
)

// Configures the specified BBB server
func (m *sshClient) ConfigureBBBServer() error {
	log.Println("Configure BBB server.")

	client, err := m.newSshClient()
	if err != nil {
		log.Println("Couldn't create SSH client:", err)
		return err
	}
	defer client.Close()

	err = m.executeGreenlightUpdate(client)
	if err != nil {
		log.Println("Couldn't execute Setup Scripts correctly:", err)
	}

	err = m.executeHetznerBBBSetupScript(client)
	if err != nil {
		log.Println("Couldn't execute Setup Scripts correctly:", err)
	}

	err = m.executeGreenlightConfiguration(client)
	if err != nil {
		log.Println("Couldn't execute Greenlight Config Scripts correctly:", err)
	}

	err = m.executeWriteApplyConfig(client)
	if err != nil {
		log.Println("Couldn't execute ApplyConfig correctly:", err)
	}

	return err
}

func (m *sshClient) executeGreenlightUpdate(client *ssh.Client) error {
	log.Println("Execute Greenlight update.")

	// send the commands
	commands := []string{
		// Update Greenlight to newest version
		"docker-compose -f /root/greenlight/docker-compose.yml pull",
		"exit",
	}

	_, err := m.runMultipleCommands(client, commands)

	return err
}

func (m *sshClient) executeHetznerBBBSetupScript(client *ssh.Client) error {
	log.Println("Execute Hetzner's BBB setup script.")

	// UserInput-Parameter of Hetzner Config-Script:
	// - Domain
	// - eMail for Let's encrypt
	// - Admin username
	// - Admin eMail
	// - Admin Password
	// - Admin Password 2nd
	// - Acknowledge of Inputs
	// - Generate Let's Encrypt Certificate
	configurationCommand := "/opt/hcloud/bbb_setup.sh <<END" + "\n" +
		config.Get().Dns.RecordName + "." + config.Get().Dns.ZoneName + "\n" +
		config.Get().Greenlight.AdminUserName + "\n" +
		config.Get().Greenlight.AdminEmail + "\n" +
		config.Get().Greenlight.AdminPassword + "\n" +
		config.Get().Greenlight.AdminPassword + "\n" +
		"Y" + "\n" +
		"Y" + "\n" +
		config.Get().Greenlight.LetsEncryptEmail + "\n" +
		"END"

	// send the commands
	commands := []string{
		configurationCommand,
		"exit",
	}

	_, err := m.runMultipleCommands(client, commands)

	return err
}

func (m *sshClient) executeGreenlightConfiguration(client *ssh.Client) error {
	log.Println("Execute Greenlight configuration.")

	greenlightEnvPath := "/root/greenlight/.env"

	configMailServer := "sed -i 's/SMTP_SERVER=.*/SMTP_SERVER=" + config.Get().Greenlight.Smtp.Server + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/SMTP_PORT=.*/SMTP_PORT=" + strconv.FormatUint(uint64(config.Get().Greenlight.Smtp.Port), 10) + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/SMTP_DOMAIN=.*/SMTP_DOMAIN=" + config.Get().Greenlight.Smtp.Domain + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/SMTP_USERNAME=.*/SMTP_USERNAME=" + config.Get().Greenlight.Smtp.Username + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/SMTP_PASSWORD=.*/SMTP_PASSWORD=" + config.Get().Greenlight.Smtp.Password + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/SMTP_AUTH=.*/SMTP_AUTH=" + config.Get().Greenlight.Smtp.Auth + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/SMTP_STARTTLS_AUTO=.*/SMTP_STARTTLS_AUTO=" + strconv.FormatBool(config.Get().Greenlight.Smtp.StartTlsAuto) + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/.*SMTP_TLS=.*/SMTP_TLS=" + strconv.FormatBool(config.Get().Greenlight.Smtp.Tls) + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/.*SMTP_SENDER=.*/SMTP_SENDER=" + config.Get().Greenlight.Smtp.Sender + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/.*SMTP_TEST_RECIPIENT=.*/SMTP_TEST_RECIPIENT=" + config.Get().Greenlight.Smtp.TestRecipient + "/g' " + greenlightEnvPath + "\n" +
		"sed -i 's/.*ALLOW_MAIL_NOTIFICATIONS=.*/ALLOW_MAIL_NOTIFICATIONS=true/g' " + greenlightEnvPath

	configRegistation := "sed -i 's/DEFAULT_REGISTRATION=.*/DEFAULT_REGISTRATION=" + config.Get().Greenlight.DefaultRegistration + "/g' " + greenlightEnvPath

	// send the commands
	commands := []string{
		configMailServer,
		configRegistation,
		"docker-compose -f /root/greenlight/docker-compose.yml down",
		"docker-compose -f /root/greenlight/docker-compose.yml up -d",
		"exit",
	}

	_, err := m.runMultipleCommands(client, commands)

	return err
}

func (m *sshClient) executeWriteApplyConfig(client *ssh.Client) error {
	log.Println("Execute Write ApplyConfig.")

	applyConfigFile := "#!/bin/bash\n\n" +
		`
# Pull in the helper functions for configuring BigBlueButton
source /etc/bigbluebutton/bbb-conf/apply-lib.sh` + "\n\n"

	if config.Get().BigBlueButton.EnableMultipleKurentos {
		applyConfigFile += "enableMultipleKurentos" + "\n\n"
	}

	applyConfigFile += "echo \"  - Set STUN-Server for Freeswitch\"" + "\n" +
		"xmlstarlet edit --inplace --update '//X-PRE-PROCESS[@cmd=\"set\" and starts-with(@data, \"external_rtp_ip=\")]/@data' " +
		"--value \"external_rtp_ip=stun:" + config.Get().BigBlueButton.StunServer + "\" /opt/freeswitch/conf/vars.xml" + "\n"

	applyConfigFile += "sed -i 's/allowStartStopRecording=.*/allowStartStopRecording=" + strconv.FormatBool(config.Get().BigBlueButton.AllowStartStopRecording) + "/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties" + "\n" +
		"sed -i 's/disableRecordingDefault=.*/disableRecordingDefault=" + strconv.FormatBool(config.Get().BigBlueButton.DisableRecordingDefault) + "/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties" + "\n"

	applyConfigFile += `echo "  - Update TURN server configuration turn-stun-servers.xml"
cat <<HERE > /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.springframework.org/schema/beans
  http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
 <bean id="stun0" class="org.bigbluebutton.web.services.turn.StunServer">
  <constructor-arg index="0" value="stun:` + config.Get().BigBlueButton.StunServer + `"/>
 </bean>
 <bean id="turn0" class="org.bigbluebutton.web.services.turn.TurnServer">
  <constructor-arg index="0" value="` + config.Get().BigBlueButton.TurnServerKey + `"/>
  <constructor-arg index="1" value="turns:` + config.Get().BigBlueButton.TurnServer + ":" + strconv.FormatUint(uint64(config.Get().BigBlueButton.TurnServerPort), 10) + `?transport=tcp"/>
  <constructor-arg index="2" value="86400"/>
 </bean>
 <bean id="stunTurnService"
	  class="org.bigbluebutton.web.services.turn.StunTurnService">
  <property name="stunServers">
	  <set>
		  <ref bean="stun0"/>
	  </set>
  </property>
  <property name="turnServers">
	  <set>
		  <ref bean="turn0"/>
	  </set>
  </property>
 </bean>
</beans>
HERE`

	writeCommand := "cat <<DOC > /etc/bigbluebutton/bbb-conf/apply-config.sh\n" +
		applyConfigFile + "\n" +
		"DOC"

	// send the commands
	commands := []string{
		writeCommand,
		"chmod +x /etc/bigbluebutton/bbb-conf/apply-config.sh",
		"bbb-conf --restart",
		"exit",
	}

	_, err := m.runMultipleCommands(client, commands)

	return err
}
