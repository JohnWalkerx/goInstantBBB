package server

import (
	"encoding/json"
	"net/http"
)

type ErrorResponse struct {
	ErrorString string `json:"errorString"`
}

func apiError(w http.ResponseWriter, status int, message string) {
	response := ErrorResponse{
		ErrorString: message,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(response)
}
