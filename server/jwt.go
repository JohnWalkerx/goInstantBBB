package server

import (
	"errors"
	"log"
	"time"

	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	"github.com/dgrijalva/jwt-go"
)

const (
	expirationTime = 24 * 60 * time.Minute
)

type JwtPayload struct {
	Username string
	Email    string
	Id       uint
}

type JwtClaims struct {
	Username string `json:"Username"`
	Email    string `json:"Email"`
	jwt.StandardClaims
}

func GenerateJwtToken(payload JwtPayload) (string, error) {
	jwtSecret := config.Get().General.JwtSecret

	if jwtSecret == "" {
		log.Fatalln("JwtSecret not provided in configration.")
	}

	key := []byte(jwtSecret)

	expTime := time.Now().Add(expirationTime)

	claims := &JwtClaims{
		Username: payload.Username,
		Email:    payload.Email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expTime.Unix(),
		},
	}

	unsignedToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signedToken, err := unsignedToken.SignedString(key)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func VerifyJwtToken(tokenStr string) (*JwtClaims, error) {
	jwtSecret := config.Get().General.JwtSecret

	if jwtSecret == "" {
		log.Fatalln("JwtSecret not provided in configration.")
	}

	key := []byte(jwtSecret)

	claims := &JwtClaims{}

	token, err := jwt.ParseWithClaims(tokenStr, claims, func(t *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return claims, errors.New("invalid token signature")
		}

		return claims, err
	}

	if !token.Valid {
		return claims, errors.New("invalid token")
	}

	return claims, nil
}
