package server

import "codeberg.org/JohnWalkerx/goInstantBBB/handler"

type UserResponse struct {
	Username string `json:"Username"`
	Token    string `json:"Token"`
}

type GetServerResponse struct {
	Name        string        `json:"Name"`
	ServerState handler.State `json:"ServerState"`
	IsServerUp  bool          `json:"IsServerUp"`
	IpAddressV4 string        `json:"IpAddressV4"`
	IpAddressV6 string        `json:"IpAddressV6"`
	DnsAddress  string        `json:"DnsAddress"`
}

type GetAllUsersResponse struct {
	Users []UserDataResponse `json:"Users"`
}

type UserDataResponse struct {
	Username string `json:"Username"`
}
