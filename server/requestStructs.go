package server

type UserCredentialsRequest struct {
	Username string `json:"Username"`
	Password string `json:"Password"`
}

type UserCreationRequest struct {
	Username string `json:"Username"`
	Password string `json:"Password"`
}

type UserDeleteRequest struct {
	Username string `json:"Username"`
}
