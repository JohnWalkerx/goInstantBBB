package server

import (
	"encoding/json"
	"net/http"

	"codeberg.org/JohnWalkerx/goInstantBBB/cloudProvider"
	"codeberg.org/JohnWalkerx/goInstantBBB/config"
	"codeberg.org/JohnWalkerx/goInstantBBB/handler"
	"codeberg.org/JohnWalkerx/goInstantBBB/userManager"
	"github.com/gorilla/mux"
)

type Server struct {
	baseRouter *mux.Router
	api        *mux.Router
	handler    *handler.Handler
}

func NewServerObject() *Server {
	server := &Server{
		baseRouter: mux.NewRouter(),
		handler:    handler.NewHandler(),
	}

	server.api = server.baseRouter.PathPrefix("/api/v1").Subrouter()

	return server
}

func (s *Server) ListenAndServe(address string) {
	http.ListenAndServe(address, s.baseRouter)
}

func (s *Server) RegisterApiHandlers() {
	s.api.HandleFunc("/bbbserver", CheckAuth(s.bbbserverGet)).Methods(http.MethodGet)
	s.api.HandleFunc("/bbbserver", CheckAuth(s.bbbserverPost)).Methods(http.MethodPost)
	s.api.HandleFunc("/bbbserver", CheckAuth(s.bbbserverDelete)).Methods(http.MethodDelete)
	s.api.HandleFunc("/bbbserver", s.bbbserverOptions).Methods(http.MethodOptions)

	s.api.HandleFunc("/auth/login", s.userLogin).Methods(http.MethodPost, http.MethodOptions)

	s.api.HandleFunc("/auth/users", CheckAuth(s.usersGet)).Methods(http.MethodGet)
	s.api.HandleFunc("/auth/users", s.bbbserverOptions).Methods(http.MethodOptions)
	s.api.HandleFunc("/auth/user/create", CheckAuth(s.userCreate)).Methods(http.MethodPost)
	s.api.HandleFunc("/auth/user/create", s.bbbserverOptions).Methods(http.MethodOptions)
	s.api.HandleFunc("/auth/user/delete", CheckAuth(s.userDelete)).Methods(http.MethodDelete)
	s.api.HandleFunc("/auth/user/delete", s.bbbserverOptions).Methods(http.MethodOptions)
}

func (s *Server) RegisterUiHandlers() {
	s.baseRouter.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("ui/public/"))))
}

func addCorsHeadersForBbbserver(w *http.ResponseWriter, r *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization")
}

func addCorsHeadersForAuthLogin(w *http.ResponseWriter, r *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding")
}

func (s *Server) bbbserverOptions(w http.ResponseWriter, r *http.Request) {
	addCorsHeadersForBbbserver(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
}

func (s *Server) bbbserverGet(w http.ResponseWriter, r *http.Request) {
	addCorsHeadersForBbbserver(&w, r)
	w.Header().Set("Content-Type", "application/json")

	cloud := cloudProvider.NewCloudProviderObject()

	existing, ipv4, ipv6 := cloud.IsBBBServerExisting()

	content := GetServerResponse{
		Name:        "BBBServer",
		ServerState: s.handler.GetState(),
		IsServerUp:  existing,
		IpAddressV4: ipv4,
		IpAddressV6: ipv6,
		DnsAddress:  config.Get().Dns.RecordName + "." + config.Get().Dns.ZoneName,
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(content)
}

// Starts the BBB server
func (s *Server) bbbserverPost(w http.ResponseWriter, r *http.Request) {
	addCorsHeadersForBbbserver(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}

	w.Header().Set("Content-Type", "application/json")

	err := s.handler.StartupBBBServer()

	if err != nil {
		apiError(w, http.StatusBadGateway, err.Error())
		return
	}

	w.WriteHeader(http.StatusAccepted)
}

// Deletes the BBB server
func (s *Server) bbbserverDelete(w http.ResponseWriter, r *http.Request) {
	addCorsHeadersForBbbserver(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	w.Header().Set("Content-Type", "application/json")

	err := s.handler.DeleteBBBServer()

	if err != nil {
		apiError(w, http.StatusBadGateway, err.Error())
		return
	}

	w.WriteHeader(http.StatusAccepted)
}

// Handles login request and returns JWT on success
func (s *Server) userLogin(w http.ResponseWriter, r *http.Request) {
	addCorsHeadersForAuthLogin(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}

	credentials := &UserCredentialsRequest{}
	json.NewDecoder(r.Body).Decode(credentials)

	if !userManager.IsUserAuthentificated(credentials.Username, credentials.Password) {
		apiError(w, http.StatusForbidden, "Invalid user or password")
		return
	}

	payload := JwtPayload{
		Username: credentials.Username,
	}

	token, err := GenerateJwtToken(payload)
	if err != nil {
		apiError(w, http.StatusInternalServerError, "Failed to generate new JWT token")
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	response := UserResponse{
		Username: credentials.Username,
		Token:    token,
	}

	json.NewEncoder(w).Encode(response)
}

func (s *Server) usersGet(w http.ResponseWriter, r *http.Request) {
	addCorsHeadersForBbbserver(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}

	usernames := userManager.ExistingUsernames()

	response := GetAllUsersResponse{}

	for _, username := range usernames {
		response.Users = append(response.Users, UserDataResponse{
			Username: username,
		})
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func (s *Server) userCreate(w http.ResponseWriter, r *http.Request) {
	addCorsHeadersForBbbserver(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}

	newUserData := &UserCredentialsRequest{}
	json.NewDecoder(r.Body).Decode(newUserData)

	err := userManager.CreateUser(newUserData.Username, newUserData.Password)

	if err != nil {
		apiError(w, http.StatusNotAcceptable, err.Error())
		return
	}

	response := UserDataResponse{
		Username: newUserData.Username,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(response)
}

func (s *Server) userDelete(w http.ResponseWriter, r *http.Request) {
	addCorsHeadersForBbbserver(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}

	deleteUser := &UserDeleteRequest{}
	json.NewDecoder(r.Body).Decode(deleteUser)

	err := userManager.DeleteUser(deleteUser.Username)

	if err != nil {
		apiError(w, http.StatusNotAcceptable, err.Error())
		return
	}

	response := UserDataResponse{
		Username: deleteUser.Username,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	json.NewEncoder(w).Encode(response)
}
